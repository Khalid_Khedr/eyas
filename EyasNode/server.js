const http = require('http');
const socketIO = require('socket.io');
const Redis = require('ioredis');
const CryptoJS = require('crypto-js');

const key = 'Eyas-key'; // Replace 'your_secret_key' with your actual secret key

const server = http.createServer();
const io = socketIO(server, {
    cors: {
        origin: "http://127.0.0.1:8000", // Adjust this to match the origin of your Vue.js application
        methods: ["GET", "POST"],
        credentials: true,
        allowEIO3: true 
    }
});

const redis = new Redis();

io.sockets.on('connection', (socket) => {
    console.log('A client connected');
    console.log(socket);
    socket.on('joinRoom', () => {
        console.log('join request');
        socket.join('eyasRoom')
    });

});

redis.subscribe('laravel_database_private-chat-channel', (err, count) => {
    if (err) {
        console.error('Failed to subscribe: ', err);
    } else {
        console.log(`Subscribed to ${count} channels`);
    }
});

redis.on('message', (channel, message) => {
    const parsedMessage = JSON.parse(message);
    console.log('Message received from Redis:', parsedMessage.data);
    console.log("channel" , channel);
    try{
    // Forward Redis messages to all connected client
    io.to('eyasRoom').emit('MessageEvent', parsedMessage.data.message_content);
    } catch (error) {
            console.error('Error:', error.message);
    }
    
});

const PORT = process.env.PORT || 2000;
server.listen(PORT, () => {
    console.log(`Node.js server listening on port ${PORT}`);
});