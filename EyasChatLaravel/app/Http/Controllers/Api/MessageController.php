<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\MessageRequest;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageEvent;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function sendMessage(MessageRequest $request){
        $data = $request->validated();
        $data['sender_id'] = Auth::user()->id;
        $message = Message::create($data);
        
       broadcast(new MessageEvent($message));
    }
}
