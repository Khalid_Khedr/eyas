<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Auth\{LoginRequest, RegisterRequest};
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    private $model;
    public function __construct(User $user)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        $this->model = $user;
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->validated();
        
        if (!$token = Auth::attempt(['phone' => $credentials['phone'], 'password' => $credentials['password']])) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        }

        $user = Auth::user();
        return response()->json([
            'user' => $user,
            'authorization' => [
                'token' => $token,
                'type' => 'bearer',
            ]
        ]);
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);
        $user = $this->model->create($data);
        return response()->json([
            'message' => 'User created successfully',
            'user' => $user
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

    public function refresh()
    {
        return response()->json([
            'user' => Auth::user(),
            'authorisation' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }
}
