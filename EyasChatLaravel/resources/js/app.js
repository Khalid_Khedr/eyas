import './bootstrap';
import Echo from 'laravel-echo';
import io from 'socket.io-client';

window.io = io;

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: 'http://127.0.0.1:2000', // Update the port to 3000
});


window.Echo.connector.socket.on('connect', () => {
    console.log('Socket connected');
});

window.Echo.connector.socket.on('disconnect', () => {
    console.log('Socket disconnected');
});


window.Echo.connector.socket.emit('joinRoom');

window.Echo.connector.socket.on('MessageEvent', (message) => {
    console.log('message recieved', message);
});